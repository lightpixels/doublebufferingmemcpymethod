#ifndef DOUBLEBUFFERINGMEMCPYMETHOD_H
#define DOUBLEBUFFERINGMEMCPYMETHOD_H

#include "doublebufferingmemcpymethod_global.h"

#include <stdint.h>

DOUBLEBUFFERINGMEMCPYMETHODSHARED_EXPORT void doubleBufferingCopy(uint16_t* pixelsFrom,
                                                                  uint16_t* pixelsTo,
                                                                  size_t    nPixels,
                                                                  uint16_t* pixelsFront,
                                                                  uint16_t* pixelsBack);

#endif // DOUBLEBUFFERINGMEMCPYMETHOD_H
