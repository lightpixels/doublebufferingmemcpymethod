#-------------------------------------------------
#
# Project created by QtCreator 2016-09-17T16:37:42
#
#-------------------------------------------------

QT       -= gui

TARGET   = DoubleBufferingMemcpyMethod

TEMPLATE = lib

include( ../deploy.pri )
DESTDIR = $$PROJECT_ROOT_DIRECTORY/deploy

DEFINES += DOUBLEBUFFERINGMEMCPYMETHOD_LIBRARY

SOURCES += doublebufferingmemcpymethod.c

HEADERS += doublebufferingmemcpymethod_global.h \
           doublebuffering.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
