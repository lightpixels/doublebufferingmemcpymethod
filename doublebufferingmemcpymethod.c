#include "doublebuffering.h"
#include <string.h>

void doubleBufferingCopy(uint16_t* pixelsFrom,  uint16_t* pixelsTo, size_t nPixels,
                         uint16_t* pixelsFront, uint16_t* pixelsBack)
{
    // 1- Set the destination FrameBuffer iterator:
    uint16_t* bufferToUpdate = pixelsTo;

    // 2- Update pixelsFront buffer:
    memcpy(pixelsFront, pixelsFrom, nPixels*sizeof(uint16_t));

    // 3- Compare pixelsFront and pixelsBack content:

    // Fast Iterators:
    uint16_t* itPixelsFront = pixelsFront;
    uint16_t* itPixelsBack = pixelsBack;

    size_t i = 0;

//#pragma omp parallel for
    for (i = 0; i < nPixels; ++i) {
        if(*itPixelsFront != *itPixelsBack) {
            *bufferToUpdate = *itPixelsFront;
        }

        ++bufferToUpdate;
        ++itPixelsFront;
        ++itPixelsBack;
    }

    // 4- Swap pixelsFront and pixelsBack buffers:
    uint16_t* tmp = pixelsBack;
    pixelsBack = pixelsFront;
    pixelsFront = tmp;
}
