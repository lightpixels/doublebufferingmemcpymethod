#ifndef DOUBLEBUFFERINGMEMCPYMETHOD_GLOBAL_H
#define DOUBLEBUFFERINGMEMCPYMETHOD_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(DOUBLEBUFFERINGMEMCPYMETHOD_LIBRARY)
#  define DOUBLEBUFFERINGMEMCPYMETHODSHARED_EXPORT Q_DECL_EXPORT
#else
#  define DOUBLEBUFFERINGMEMCPYMETHODSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // DOUBLEBUFFERINGMEMCPYMETHOD_GLOBAL_H
